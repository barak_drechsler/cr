<?php

namespace Adwords\Services;

use Adwords\DAO\ReportDAO;
use Adwords\DAO\PeriodicalReportDAO;
use Adwords\Entities\Report;
use Adwords\Entities\User;

use AdWordsUser;
use Selector;
use Paging;
use OrderBy;
use ReportUtils;
use Predicate;
use ReportDefinition;

class ReportService
{
    const CSV_FILE = "storage/csv/periodical_report.csv";

    protected $reportDAO;
    protected $periodicalReportDAO;
    protected $reportEntity;
    protected $csvService;

    public function __construct(ReportDAO $reportDAO, Report $reportEntity, CsvService $csvService, PeriodicalReportDAO $periodicalReportDAO)
    {
        $this->periodicalReportDAO = $periodicalReportDAO;
        $this->reportDAO = $reportDAO;
        $this->reportEntity = $reportEntity;

        $this->csvService = $csvService;
    }

    public function convertToEntity()
    {
    }



    /**
     * Get specific report by id
     * @param $id
     * @return Report
     */
    public function getReportById($id)
    {
        return $this->periodicalReportDAO->getReportByReportIdFk($id);
    }



    /**
     * Get reports by account id
     *
     * @param $id
     * @return array
     */
    public function getReportsByAccountId($id){
        return $this->reportDAO->getReportsByAccountId($id);
    }



    /**
     * Get reports by mcc id
     *
     * @param $id
     * @return array
     */
    public function getReportsByMccId($id){
        return $this->reportDAO->getReportsByMccId($id);
    }



    /**
     * Get all mcc's reports
     *
     * @return array
     */
    public function getAllReports()
    {
        return $this->reportDAO->getAllReports();
    }



    /**
     * Get reports from adwords
     *
     * @param $id
     * @param $date
     * @param $network
     * @param $device
     */
    public function getReportFromAdwords($id, $date, $network, $device){
        $user = new AdWordsUser(config_path('googleleads/adwords.ini'));
        $user->SetClientCustomerId($id);

        $filePath = base_path(ReportDAO::CSV_FILE);

        $user->LoadService('ReportDefinitionService', 'v201509');

        // Optional: Set clientCustomerId to get reports of your child accounts
        // $user->SetClientCustomerId('INSERT_CLIENT_CUSTOMER_ID_HERE');
        // Create selector.
        $selector = new Selector();
        $selector->fields = array('Date', 'CampaignId', 'CampaignName', 'Clicks', 'Impressions', 'Ctr', 'AverageCpc', 'AverageCpm',
            'AveragePosition', 'ConvertedClicks', 'CostPerConvertedClick', 'ClickConversionRate', 'ViewThroughConversions',
            'AdNetworkType2', 'Device', 'Cost');

        // Optional: use predicate to filter out paused criteria.
        $selector->predicates[] = new Predicate('Status', 'NOT_IN', array('PAUSED', 'REMOVED'));

        if($device != null){
            if($device == "UNKNOWN" || $device == "DESKTOP" || $device == "HIGH_END_MOBILE" || $device == "TABLET"){
                $selector->predicates[] = new Predicate('Device', 'EQUALS', array($device));
            }
        }

        if($network != null){
            if($network == "UNKNOWN" || $network == "SEARCH" || $network == "YOUTUBE_WATCH" || $network == "CONTENT"){
                $selector->predicates[] = new Predicate('AdNetworkType2', 'EQUALS', array($network));
            }
        }


        // Create report definition.
        $reportDefinition = new ReportDefinition();
        $reportDefinition->selector = $selector;
        $reportDefinition->reportName = 'Criteria performance report #' . uniqid();

        if($date != null && ($date == "TODAY" || $date == "LAST_7_DAYS" || $date == "THIS_MONTH")){
            $reportDefinition->dateRangeType = $date;
        } else {
            $reportDefinition->dateRangeType = 'TODAY';
        }

        $reportDefinition->reportType = 'CRITERIA_PERFORMANCE_REPORT';
        $reportDefinition->downloadFormat = 'CSV';

        // Exclude criteria that haven't recieved any impressions over the date range.
        $reportDefinition->includeZeroImpressions = true;
        // Set additional options.
        $options = array('version' => 'v201509');
        // Optional: Set skipReportHeader, skipColumnHeader, skipReportSummary to
        //     suppress headers or summary rows.
        // $options['skipReportHeader'] = true;
        // $options['skipColumnHeader'] = true;
        // $options['skipReportSummary'] = true;
        // Optional: Set includeZeroImpressions to include zero impression rows in
        //     the report output.
        // $options['includeZeroImpressions'] = true;

        // Download report.
        ReportUtils::DownloadReport($reportDefinition, $filePath, $user, $options);
    }


    /**
     * Insert report from adwords to DB
     *
     * @param User $user
     * @param $mccId
     * @param $accountId
     * @param $reportName
//     */
//    public function insertReportFromAdwords(User $user, $mccId, $accountId, $reportName){
//        return $this->reportDAO->insertReportFromAdwords($user, $mccId, $accountId, $reportName);
//    }


    /**
     * Insert reports from adwords
     *
     * @param User $user
     * @param $mccId
     * @param $accountId
     * @param $reportName
     * @param $device
     * @param $network
     * @param $date
     */
    public function insertReportFromAdwords(User $user, $mccId, $accountId, $reportName, $device, $network, $date, $networkDescription){
        $file = self::CSV_FILE;
        $csvFile  = $this->csvService->getFile($file);

        // Set new report
        $report = $this->reportDAO;

        $report->setName($reportName);
        $report->setCustomerIdFk(1);
        $report->setUserIdFk(1);
        $report->setMccId($mccId);
        $report->setAccountId($accountId);
        $report->setCampaignId(1);
        $report->setDescription($networkDescription);


        // Insert new report
        $report = $this->reportDAO->insertReport($report);

        // Insert all the periodical reports
        for($i = 2; $i < count($csvFile) - 2; $i++){
            // Set a new periodical report
            $periodicalReport = $this->periodicalReportDAO;

            $periodicalReport->setReportIdFk($report->getId());
            $periodicalReport->setClicks($csvFile[$i][3]);
            $periodicalReport->setImpression($csvFile[$i][4]);
            $periodicalReport->setCtr($csvFile[$i][5]);
            $periodicalReport->setAvgCpc($csvFile[$i][6]);
            $periodicalReport->setAvgCmp($csvFile[$i][7]);
            $periodicalReport->setAvgPosition($csvFile[$i][8]);
            $periodicalReport->setConvertedClicks($csvFile[$i][9]);
            $periodicalReport->setCostConvertedClicks($csvFile[$i][10]);
            $periodicalReport->setClickConversionsRate($csvFile[$i][11]);
            $periodicalReport->setViewThroughConversions($csvFile[$i][12]);
            $periodicalReport->setNetwork($csvFile[$i][13]);
            $periodicalReport->setDevice($csvFile[$i][14]);
            $periodicalReport->setCost($csvFile[$i][15]);
            $periodicalReport->setDate($csvFile[$i][0]);

            // Insert new periodical report
            $periodicalReport->insertPeriodicalReport($periodicalReport);
        }
    }

    /**
     * Set the date data readable to the client side
     *
     * @param $network
     * @return string
     */
    public function dateReadable($network){
        if($network == "TODAY"){
            return "Daily";
        } else if($network == "LAST_7_DAYS"){
            return "Weekly";
        } else {
            return "Monthly";
        }
    }

}